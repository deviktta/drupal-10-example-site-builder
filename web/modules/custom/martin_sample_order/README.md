## Sample Order

### What's inside the folder ?

- Folder /src which contains a Form to request a Sample, and a Controller to send handle it
- Folder /templates where my view actually comes from
- File .info.yml to specify the module informations
- File .module where a hook_theme lives to get the right template and informations
- File .routing.yml to define the route access and specifications

### Specifications

The Form is accessible throught the path __/sample-order__. When you have filled it in,
a sample order content has been created so it can be reviewed.
