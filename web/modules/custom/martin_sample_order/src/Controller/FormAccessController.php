<?php

namespace Drupal\martin_sample_order\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Form Controller.
 */
class FormAccessController extends ControllerBase
{

  /**
   * Display the form according to the user.
   */
    public static function content()
    {
        $current_user = \Drupal::currentUser();
        $login_route = Url::fromRoute('user.login')->toString();
        $form = \Drupal::formBuilder()->getForm('\Drupal\martin_sample_order\Form\SampleOrderForm');
        $access_form = false;
        $node_storage = \Drupal::entityTypeManager()->getStorage('node');
        $query = $node_storage->getQuery();
        // Check if user has already make a request.
        $query->accessCheck(true)
          ->condition('status', 1)
          ->condition('type', 'sample_order')
          ->condition('field_email', $current_user->getEmail());
        $count = count($query->execute());

        // If anonymous, can't request a sample.
        if ($current_user->isAnonymous()) {
            return new RedirectResponse($login_route);
        } elseif ($count > 0) {
            return $access_form;
        } else {
            $access_form = true;
            return [
              '#theme' => 'sample',
              '#title' => 'Sample request',
              '#form' => $form,
              '#access' => $access_form,
            ];
        }
    }
}
