<?php

namespace Drupal\martin_sample_order\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\node\Entity\Node;

/**
 * Form of sample type.
 */
class SampleOrderForm extends FormBase
{

    private $entityTypeManager;
    protected $messenger;
    private $language;

    public function __construct(
        EntityTypeManagerInterface $entityTypeManager,
        MessengerInterface $messenger,
        LanguageManager $language
    ) {
        $this->entityTypeManager = $entityTypeManager;
        $this->messenger = $messenger;
        $this->language = $language;
    }

  /**
   * {@inheritdoc}
   */
    public function getFormId()
    {
        return 'martin_sample_order';
    }

  /**
   * {@inheritdoc}
   */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $productNodes = $this->entityTypeManager->getStorage('node')->loadByProperties(['type' => 'product']);
        $options = [];
        foreach ($productNodes as $node) {
            $options[$node->id()] = $node->title->value;
        }

        $form['sample-order'] = [
          '#type' => 'fieldset',
          '#title' => t('Order information'),
        ];
        $form['sample-order']['product'] = [
          '#type' => 'select',
          '#options' => $options,
          '#title' => t('Choose a Product'),
          '#required' => true,
        ];
        $form['sample-order']['name'] = [
          '#type' => 'textfield',
          '#title' => t('Firstname'),
          '#required' => true,
        ];
        $form['sample-order']['lastname'] = [
          '#type' => 'textfield',
          '#title' => t('Lastname'),
          '#required' => true,
        ];
        $form['sample-order']['email'] = [
          '#type' => 'textfield',
          '#title' => t('Email'),
          '#required' => true,
        ];
        $form['sample-order']['address'] = [
          '#type' => 'textfield',
          '#title' => t('Address'),
          '#required' => true,
        ];
        $form['sample-order']['city'] = [
          '#type' => 'textfield',
          '#title' => t('City/Town'),
          '#required' => true,
        ];
        $form['sample-order']['zipcode'] = [
          '#type' => 'textfield',
          '#title' => t('Zipcode'),
          '#required' => true,
        ];
        $form['sample-order']['country'] = [
          '#type' => 'textfield',
          '#title' => t('Country'),
          '#required' => true,
        ];
        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => t('Place request'),
        ];

        return $form;
    }

  /**
   * {@inheritdoc}
   */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $language = $this->language->getCurrentLanguage()->getId();
        $today = time();
        $firstname = $form_state->getValue('firstname');
        $lastname = $form_state->getValue('lastname');
        $email = $form_state->getValue('email');
        $address = $form_state->getValue('address');
        $city = $form_state->getValue('city');
        $zipcode = $form_state->getValue('zipcode');
        $country = $form_state->getValue('country');
        $product = $form_state->getValue('product');
        $chosen_product = $this->entityTypeManager->getStorage('node')->load($product);
        $quantity = $chosen_product->get('field_quantity')->getValue()[0]['value'];

        $node = Node::create([
          'nid' => null,
          'type' => 'sample_order',
          'title' => '#' . $firstname . '' . $lastname,
          'uid' => 1,
          'created' => $today,
          'changed' => $today,
          'status' => true,
          'langcode' => $language,
          'field_product' => $product,
          'field_firstname' => $firstname,
          'field_lastname' => $lastname,
          'field_email' => $email,
          'field_address' => $address,
          'field_city' => $city,
          'field_zipcode' => $zipcode,
          'field_country' => $country,
        ]);

        $node->save();
        $form_state->setRedirect('<front>');
        $this->messenger->addMessage(
            'Request successfully sent : ' . $chosen_product->title->value . ' containing ' . $quantity . ' ml.'
        );
    }
}
