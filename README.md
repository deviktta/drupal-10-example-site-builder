# Drupal 10 Example Site Builder

## Local development (Docksal)

### First installation setup instructions

1. Use the main development Git branch:
    ```shell
    git checkout develop
    ```

2. (Optional) If needed, override Docksal configuration via `.docksal/docksal-local.env`.

3. Start the Docksal project by running `fin start`:
    ```shell
    fin start
    ```

4. Generate a new `/web/sites/development/settings.local.php` by copying the one in `/secrets/`.

5. Import a Drupal database (present in `/secrets/` directory):
    ```shell
    zcat < secrets/XXXX.sql.gz | fin db import
    ```

6. Do a generic Drupal update by running:
    ```shell
    fin drupal-update
    ```


## Credentials

**IMPORTANT! only the ones that can be public**

| User           | Password | Roles            | Notes                                  |
|----------------|----------|------------------|----------------------------------------|
| admin          | $3cr3T   | administrator    | Super admin                            |
| Don Ibai       | $3cr3T   | content_editor   | Content editor                         |
| Angry Redditor | $3cr3T   | moderator        | Allowed to moderate comments           |
| Leeroy Jenkins | $3cr3T   | user_tier_gold   | Commentor with full-rights             |
| Mathias Shaw   | $3cr3T   | user_tier_silver | Commentor without approval, can't edit |
| Hogger         | $3cr3T   | user_tier_bronze | Commentor under approval               |

## Routing

| Route name               | Path          | Description                 |
|--------------------------|---------------|-----------------------------|
| martin_sample_order.form | /sample-order | Display sample request form |
